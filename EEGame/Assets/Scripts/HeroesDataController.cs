﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.JSONData;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class HeroesDataController : MonoBehaviour
    {
        private Text heroName;
        private Text heroDesc;
        private Image heroImage;
        private Button leftBtn;
        private Button rightBtn;
        private Button startButton;
        private List<Hero> heroes;
        private int heroIndex;
        private List<Sprite> heroesImages;

        private int CurrentHeroIndex
        {
            get { return heroIndex; }
            set
            {
                if (heroes == null)
                {
                    Debug.Log("Heroes list wasn't instantiated");
                    return;
                }
                if (heroes.Count == 0 || value >= heroes.Count)
                {
                    heroIndex = 0;
                }
                else
                {
                    if (value < 0)
                    {
                        heroIndex = heroes.Count - 1;
                    }
                    else
                    {
                        heroIndex = value;
                    }
                }
                ShowHeroInfo();
            }
        }

        private void Start()
        {
            heroName = GameObject.Find("HeroName").GetComponent<Text>();
            heroDesc = GameObject.Find("HeroDesc").GetComponent<Text>();
            heroImage = GameObject.Find("HeroImage").GetComponent<Image>();
            heroes = GameController.PullJSON<Hero>("Heroes");
            if (heroes != null)
            {
                heroesImages = LoadImgForHeroes();
                ShowHeroInfo();
            }
            InitBtns();

        }

        private void InitBtns()
        {
            var canvas = GameObject.Find("Canvas");
            var leftBtnPrefab = Resources.Load<Button>("Prefab/LeftBtn");
            if (leftBtnPrefab != null)
            {
                leftBtn = Instantiate(leftBtnPrefab);
                leftBtn.onClick.AddListener(LeftBtnClick);
                leftBtn.transform.SetParent(canvas.transform);
                leftBtn.transform.localPosition = new Vector3(-canvas.GetComponent<RectTransform>().rect.width / 3, 0);
                leftBtn.image.rectTransform.sizeDelta = new Vector2(100, 100);
            }
            else
            {
                Debug.Log("LeftBtn Prefab is null");
            }
            var rightBtnPrefab = Resources.Load<Button>("Prefab/RightBtn");
            if (rightBtnPrefab != null)
            {
                rightBtn = Instantiate(rightBtnPrefab);
                rightBtn.onClick.AddListener(RightBtnClick);
                rightBtn.transform.SetParent(canvas.transform);
                rightBtn.transform.localPosition = new Vector3(canvas.GetComponent<RectTransform>().rect.width / 3, 0);
                rightBtn.image.rectTransform.sizeDelta = new Vector2(100, 100);
            }
            else
            {
                Debug.Log("RightBtn Prefab is null");
            }
            var startBtnPrefab = Resources.Load<Button>("Prefab/StartGameBtn");
            if (startBtnPrefab != null)
            {
                startButton = Instantiate(startBtnPrefab);
                startButton.onClick.AddListener(StartGameScene);
                startButton.transform.SetParent(canvas.transform);
                startButton.transform.localPosition = new Vector3(canvas.GetComponent<RectTransform>().rect.width / 3, -canvas.GetComponent<RectTransform>().rect.height / 3);
            }

        }

        /// <summary>
        /// Fills list with images with names from "Heroes"pool
        /// </summary>
        /// <returns>Filled list(if possible)</returns>
        private List<Sprite> LoadImgForHeroes()
        {
            if (heroes == null)
                return new List<Sprite>();
            var resultList = new List<Sprite>();
            foreach (var hero in heroes)
            {
                resultList.Add(LoadImage(hero.Name));
            }
            return heroes.Select(h => LoadImage(h.Name)).ToList();
        }

        /// <summary>
        /// Method to load and return hero image 
        /// </summary>
        /// <param name="imageName">Image name, must be same as heroes name</param>
        /// <returns>Loaded image </returns>
        public static Sprite LoadImage(string imageName)
        {
            var image = Resources.Load<Sprite>("HeroesImg/" + imageName);
            if (image != null)
            {
                return image;
            }
            Debug.Log("Cannot find  hero image with name:" + imageName);
            return new Sprite();
        }

        private void LeftBtnClick()
        {
            CurrentHeroIndex--;
        }

        private void RightBtnClick()
        {
            CurrentHeroIndex++;
        }

        private void ShowHeroInfo()
        {
            heroName.text = heroes[heroIndex].Name;
            heroDesc.text = heroes[heroIndex].Description;
            heroImage.sprite = heroesImages[heroIndex];
        }

        private void StartGameScene()
        {
            if (heroes == null)
            {
                Debug.Log("Heroes data is null, cannot start a new game without a hero");
                return;
            }
            var hero = heroes[CurrentHeroIndex];
            var dataHolder = Instantiate(new GameObject());
            dataHolder.AddComponent<MenuDataHolder>();
            dataHolder.name = "HeroData";
            dataHolder.GetComponent<MenuDataHolder>().HeroData = hero;
            DontDestroyOnLoad(dataHolder);

            SceneManager.LoadScene(1);

        }
        // Update is called once per frame
        private void Update()
        {
		    
        }
    }
}
