using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.JSONData;
using Assets.Scripts.JSONData.Standart;
using UnityEngine;
using System;

namespace Assets.Scripts
{
    public class HeroController : MonoBehaviour
    {
        public event Action<List<string>> OnEffectsChange;

        public Hero hero;

        public List<Effect> Effects { get; set; }

        public string Name { get; set; }

        public const int MaxStr = 30;
        public const int MaxInt = 30;

        public int GlobalGold
        { 
            get { return globalGold; }
        }

        private int globalGold;

        private readonly string globalGoldKey = "GlobalGold";

        public GameObject statusBarPrefab;
        public Canvas canvas;

        private StatusBarController globalGoldBar;
        private StatusBarController staminaBarController;
        private StatusBarController kindnessStatusBar;
        private int intellect;
        private StatusBarController goldStatusBar;
        private int gold;
        private GameController gameController;

        public GameController GameController
        {
            get
            {
                if (gameController == null)
                {
                    gameController = GameObject.Find("GameManager").GetComponent<GameController>();
                }
                return gameController;
            }
        }

        private int strength;

        public int Gold
        {
            get { return gold; }
        }

        public int Str
        {
            get { return strength; }
        }

        public int Int
        {
            get { return intellect; }
        }

        private int kindness;

        public int Kindness
        {
            get { return kindness; }
        }

        private int stamina;

        public int Stamina
        {
            get { return stamina; }
        }

        public void ChangeStrength(int value)
        {
            strength += value; 
        }

        public void ChangeIntellect(int value)
        {
            intellect += value;
        }

        public void ChangeStamina(int value)
        {
            stamina += value;
            staminaBarController.Value = stamina; 
        }

        public void ChangeKindness(int value)
        {
            kindness += value;
            kindnessStatusBar.Value = kindness;
        }

        public void ChangeGlobalGold(int value)
        {
            globalGold += value;
            globalGoldBar.Value = globalGold; 
            PlayerPrefs.SetInt(globalGoldKey, globalGold);
            PlayerPrefs.Save();
        }

        public void ChangeGold(int value)
        {
            gold += value;
            if (gold < 0)
            {
                gold = 0;
            }
            goldStatusBar.Value = gold;
        }

        private void Start()
        {
            if (Effects == null)
                Effects = new List<Effect>();
            statusBarPrefab = Resources.Load<GameObject>("Prefab/StatusBar");
            canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
            if (statusBarPrefab == null || canvas == null)
                throw new NullReferenceException("Error in loading statusBarPrefab or canvas finding");
            
            globalGold = PlayerPrefs.GetInt(globalGoldKey);

            var statusBarRect = statusBarPrefab.GetComponent<RectTransform>();
            var canvasRect = canvas.GetComponent<RectTransform>();
            var statusX = -canvasRect.rect.width / 2 + statusBarRect.rect.width / 2;
            var statusY = canvasRect.rect.height / 2 - statusBarRect.rect.height / 2;

            staminaBarController = InitStatusBar(statusX, statusY, "Stamina", 100, 0, "byceps");

            kindnessStatusBar = InitStatusBar(statusX + statusBarRect.rect.width, statusY, "Kindness", 100, 0, "angel");

            goldStatusBar = InitStatusBar(statusX + statusBarRect.rect.width * 2, statusY, "GOLD", 0, 0, "money");

            globalGoldBar = InitStatusBar(statusX + statusBarRect.rect.width * 3, statusY, "GlobalGold", 0, globalGold, "trophy");
            InitHero();
        }

        public bool CheckStatRequirements(StatRequirements requirements)
        {
            if (requirements == null)
                return true;
            return requirements.GOLD <= Gold && requirements.INT <= Int
            && requirements.Kindness <= Kindness && requirements.Stamina <= Stamina
            && requirements.STR <= Str;
        }

        public bool CheckEffectsAll(List<string> neededEffects)
        {
            var desiredEffects = GameController.GetEffectsByNames(neededEffects);

            if (Effects == null)
                Effects = new List<Effect>();

            if (desiredEffects == null)
                return true;

            return desiredEffects.All(effect => Effects.Contains(effect));
        }

        public bool CheckEffectsNone(List<string> effects)
        {
            var desiredEffects = GameController.GetEffectsByNames(effects);
            return !desiredEffects.Any(effect => Effects.Contains(effect));
        }

        //TODO REVERT CHANGES IF EXCEPTION WAS THROWN
        public bool TryApplyStatChange(CharacteristicsChange change)
        {
            if (change == null)
                return true;
            try
            {
                ChangeStrength(change.STR);
                ChangeIntellect(change.INT);
                ChangeGold(change.Gold);
                ChangeStamina(change.RequiredStamina);
                ChangeKindness(change.RequiredKindness);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveEffects(IEnumerable<Effect> effectsToRemove)
        {
            Effects.RemoveAll(effectsToRemove.Contains);
            UpdateVisibleEffects();
            return RevertEffectStats(effectsToRemove);
        }

        public bool RevertEffectStats(IEnumerable<Effect> effects)
        {
            foreach (var stats in effects.Select(e => e.EffectCharacteristics))
            {
                stats.STR = -stats.STR;
                stats.INT = -stats.INT;
                stats.Gold = -stats.Gold;
                stats.RequiredKindness = -stats.RequiredKindness;
                if (!TryApplyStatChange(stats))
                {
                    return false;
                }
            }
            return true;
        }

        public bool RemoveEffectsByName(IEnumerable<string> namesToRemove)
        {
            return RemoveEffects(Effects.Where(effect => namesToRemove.Contains(effect.Name)).ToList());
        }

        public bool AttachEffects(IEnumerable<string> newEffects)
        {
            if (newEffects == null || !newEffects.Any())
                return true;
            
            var effectsToAttach = GameController.GetEffectsByNames(newEffects.ToList());
            foreach (var effect in effectsToAttach.Where(effect => !Effects.Contains(effect)))
            {
                if (!TryApplyStatChange(effect.EffectCharacteristics))
                {
                    return false;
                }
                Effects.Add(effect);
            }
            UpdateVisibleEffects();
            return true;
        }

        public void CleanUpEffects()
        {
            if (Effects == null)
                return;
            Effects.RemoveAll(effect => !effect.IsLongterm);
            UpdateVisibleEffects();
        }

        private void UpdateVisibleEffects()
        {
            var currentEffects = Effects.Where(e => e.EffectType != null && e.EffectType.Contains("Visible"))
                .Select(e => e.Name).ToList();
            OnEffectsChange.SafeInvoke(currentEffects);
        }

        private void InitHero()
        {
            if (hero == null)
            {
                throw new NullReferenceException("Hero wasn't seted");
            }
            Name = hero.Name;
            ChangeIntellect(hero.INT);
            ChangeStrength(hero.STR);
            ChangeGold(hero.GOLD);
            ChangeStamina(hero.STAMINA);
            ChangeKindness(hero.KINDNESS);
        }

        private StatusBarController InitStatusBar(float statusX, float statusY, string barName, int maxValue, int initValue, string iconName = "")
        {
            var statusBarObject = Instantiate(statusBarPrefab,
                statusBarPrefab.transform.position,
                statusBarPrefab.transform.rotation,
                canvas.transform);

            statusBarObject.transform.localPosition = new Vector2(statusX, statusY);

            var statusBarController = statusBarObject.GetComponent<StatusBarController>();
            statusBarController.Name = barName;
            statusBarController.IconName = iconName;
            statusBarController.MaxValue = maxValue;
            statusBarController.Value = initValue;
            return statusBarController;
        }
    }
}