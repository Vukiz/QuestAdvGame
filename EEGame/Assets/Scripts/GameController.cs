﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Assets.Scripts.JSONData;
using Assets.Scripts.JSONData.Generic;
using Assets.Scripts.JSONData.Standart;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameController : MonoBehaviour
    {
        public List<Encounter> EncountersPool = new List<Encounter>();
        public List<StandartVariant> VariantsPool = new List<StandartVariant>();
        public List<GenericVariant> GenericVariantsPool = new List<GenericVariant>();
        public List<StandartResult> ResultsPool = new List<StandartResult>();
        public List<Effect> EffectsPool = new List<Effect>();
        public List<Ending> EndingsPool = new List<Ending>(); 

        // Use this for initialization
        private void Start()
        {
            FillPools();
            InitHero();
            GetComponent<GameSessionHandler>().StartGameSession();
        }

        private void InitHero()
        {
            var heroData = GameObject.Find("HeroData");
            var heroController = GameObject.Find("Hero").GetComponent<HeroController>();
            if (heroData == null || heroController == null)
                return;
            var hero = heroData.GetComponent<MenuDataHolder>().HeroData;
            heroController.hero = hero;
            heroController.Effects = EffectsPool.GetJoinByName(hero.InitialEffects);
            Destroy(heroData);
            GameObject.Find("SideBarHud").GetComponent<HudController>().HandleEffectChanges(heroController.Effects
                .Select(effect => effect.Name).ToList());
        }
        // Update is called once per frame
        private void Update()
        {

        }

        public void FillPools()
        {
            EncountersPool = PullJSON<Encounter>("Encounters");
            VariantsPool = PullJSON<StandartVariant>("Variants");
            ResultsPool = PullJSON<StandartResult>("Results");
            EffectsPool = PullJSON<Effect>("Effects");
            EndingsPool = PullJSON<Ending>("Endings");
        }

        public void AddVariantsToGenPool(List<GenericVariant> variants)
        {
            foreach (var variant in variants.Where(variant => GenericVariantsPool.All(x => x.Name != variant.Name)))
            {
                GenericVariantsPool.Add(variant);
            }
        }

        public IEnumerable<Effect> GetEffectsByNames(List<string> effectNames)
        {
            return EffectsPool.GetJoinByName(effectNames);
        }

        public static List<T> PullJSON<T>(string fileName) where T : BasicObject
        {
            var dataFile = Resources.Load(fileName) as TextAsset;
            if (dataFile == null)
            {
                throw new NullReferenceException("datafile" + fileName + " is missing");
            }
            var data = JsonUtility.FromJson<Data<T>>(dataFile.text);
            return data == null || data.Objects == null ? new List<T>() : data.Objects;
        }

        public string GetEffectDescription(string eName)
        {
            foreach (var effect in EffectsPool.Where(effect => effect.Name == eName))
            {
                return effect.Description;
            }
            Debug.Log("Cannot find desired effect returned empty string");
            return "";
        }
    }
}