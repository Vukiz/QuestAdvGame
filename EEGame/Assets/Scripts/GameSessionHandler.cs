﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Assets.Scripts.JSONData;
using Assets.Scripts.JSONData.Standart;
using Assets.Scripts.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public enum GameSessionState
    {
        WaitingForStart,
        WaitingForEncounter,
        WaitingForPlayersChoice,
        EnteringStore,
        ShoppingStore,
        LeavingStore,
        ProcessingResult,
        ProcessingEnding
    }

    public class GameSessionHandler : MonoBehaviour
    {
        private Text descriptionText;
        private Button newGameButton;
        private Button exitButton;
        private List<Button> variantButtons;
        private Text levelStatusText;

        private GameController gameController;
        private HeroController heroController;
        private GameObject resultPanel;

        private bool enabledEncounterAndVariants;
        private bool enabledResultDescription;
        private bool enabledEndingDescription;

        private List<Encounter> gameSessionEncounters;
        private Encounter currentEncounter;
        private Encounter nextEncounter;

        private int groupId;
        private int currentEncListCount;
        private int currentEncCount;

        private GameSessionState currentState = GameSessionState.WaitingForStart;

        private const int NonLocationGroupId = 1;
        private const int SessionEncountersCount = 10;

        GameObject storePanel;
        private bool isPlayerLost = false;


        private int currentGambleBet = 0;
        private bool gambling = false;

        private void InitUIObj()
        {
            if (descriptionText == null)
                descriptionText = GameObject.Find("DescriptionText").GetComponent<Text>();
            newGameButton = GameObject.Find("NewGameBtn").GetComponent<Button>();
            exitButton = GameObject.Find("ExitGameBtn").GetComponent<Button>();

            if (storePanel == null)
            {
                var prefabobject = Resources.Load<GameObject>("Prefab/Store/StorePanel");
                storePanel = Instantiate(prefabobject,
                    prefabobject.transform.position,
                    prefabobject.transform.rotation,
                    GameObject.Find("Canvas").transform);
                storePanel.transform.localPosition = new Vector2(0, 0);
            }

            storePanel.SetActive(false);

            if (variantButtons == null)
            {
                var variantsHolder = GameObject.Find("Variants").transform;
                variantButtons = new List<Button>();
                for (var i = 0; i < variantsHolder.childCount; i++)
                {
                    variantButtons.Add(variantsHolder.GetChild(i).GetComponent<Button>());
                }
            }
            if (levelStatusText == null)
                levelStatusText = GameObject.Find("LevelText").GetComponent<Text>();
        }

        public void StartNewGame()
        {
            if (isPlayerLost)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                StartGameSession();
            }
        }

        public void StartGameSession()
        {
            InitUIObj();

            gameController = GameObject.Find("GameManager").GetComponent<GameController>();
            heroController = GameObject.Find("Hero").GetComponent<HeroController>();

            if (enabledEncounterAndVariants)
                ChangeVisibilityEncounterAndVariants();

            enabledResultDescription = false;

            if (enabledEndingDescription)
                ChangeVisibilityEnding();

            FormGameSessionEncountersList();
            GetCurrentEncounter();
        }

        private void Update()
        {
            HandleCurrentState();
        }

        private void HandleCurrentState()
        {
            switch (currentState)
            {
                case GameSessionState.WaitingForEncounter:
                    if (enabledResultDescription)
                        ChangeVisibilityResult();
                    GetCurrentEncounter();
                    break;
                case GameSessionState.EnteringStore:
                    if (enabledResultDescription)
                        ChangeVisibilityResult();
                    storePanel.gameObject.SetActive(true);
                    storePanel.GetComponent<StoreController>().Init();
                    ChangeState(GameSessionState.ShoppingStore);
                    break;
                case GameSessionState.ShoppingStore:
                    break;
                case GameSessionState.LeavingStore:
                    storePanel.gameObject.SetActive(false);
                    ChangeState(GameSessionState.WaitingForEncounter);
                    break;
                case GameSessionState.WaitingForPlayersChoice:
                    if (!enabledEncounterAndVariants)
                        ChangeVisibilityEncounterAndVariants();
                    break;

                case GameSessionState.ProcessingResult:
                    if (enabledEncounterAndVariants)
                        ChangeVisibilityEncounterAndVariants();
                    break;

                case GameSessionState.ProcessingEnding:
                    if (!enabledEndingDescription)
                        ChangeVisibilityEnding();
                    if (enabledEncounterAndVariants)
                        ChangeVisibilityEncounterAndVariants();
                    if (!enabledResultDescription)
                        ChangeVisibilityResult();
                    break;
            }
        }

        private void ChangeVisibilityEncounterAndVariants()
        {
            foreach (var button in variantButtons)
            {
                if (button.GetComponent<ButtonHandler>().GetAttachedVariant() == null)
                {
                    button.gameObject.SetActive(false);
                    button.GetComponentInChildren<Text>().enabled = false;
                }
                else
                {
                    button.gameObject.SetActive(!enabledEncounterAndVariants);
                    button.GetComponentInChildren<Text>().enabled = !enabledEncounterAndVariants;
                }
            }

            enabledEncounterAndVariants = !enabledEncounterAndVariants;
        }

        private void ChangeVisibilityResult()
        {
            enabledResultDescription = !enabledResultDescription;
        }

        private void ChangeVisibilityEnding()
        {
            newGameButton.gameObject.SetActive(!enabledEndingDescription);
            newGameButton.GetComponentInChildren<Text>().enabled = !enabledEndingDescription;
            exitButton.gameObject.SetActive(!enabledEndingDescription);
            exitButton.GetComponentInChildren<Text>().enabled = !enabledEndingDescription;

            enabledEndingDescription = !enabledEndingDescription;
        }

        public void ChangeState(GameSessionState newState)
        {
            currentState = newState;
        }

        private int GetRandomThroughDist(List<int> distribution)
        {
            var rand = (int)(Random.value * distribution.Sum());
            var s = 0;
            for (var i = 0; i < distribution.Count; i++)
            {
                if (distribution[i] == 0)
                    continue;
                if (rand < distribution[i] + s)
                {
                    return i;
                }
                s += distribution[i];
            }
            return distribution.Count - 1;
        }

        private void FormGameSessionEncountersList()
        {
            gameSessionEncounters = new List<Encounter>();
            groupId = GetRandomThroughDist(new List<int> { 50, 50 }) + 2;
            //+1 cause non-locationgroup id is 1 so we should move to locations and +1 cause we start from 1 not 0
            //loads to gamesessionencounters all encounters from nonlocation and all from chosen location ( groupid)
            gameSessionEncounters.AddRange(gameController.EncountersPool.Where(encounter => encounter.Id / 100 == groupId || encounter.Id / 100 == NonLocationGroupId));
            currentEncListCount = gameSessionEncounters.Count > SessionEncountersCount ? SessionEncountersCount : gameSessionEncounters.Count;
            currentEncCount = -1;
            UpdateLevelStatus();

            gameSessionEncounters.Add(new Encounter { Id = groupId * 100, Name = "Store", Rate = 100 });
        }
        /// <summary>
        /// chooses random encounter from location or non-location group
        /// </summary>
        /// <returns>encounter chosen to be currentEncounter</returns>
        private Encounter RandomCurrentEncounter()
        {
            int subGroupId;
            var locationQuests = gameSessionEncounters.Count(enc => enc.Id / 100 == groupId);
            var nonLocationQuests = gameSessionEncounters.Count(enc => enc.Id / 100 == NonLocationGroupId);
            if (nonLocationQuests == 0)
            {
                subGroupId = groupId;
            }
            else
            {
                if (locationQuests == 0)
                {
                    subGroupId = NonLocationGroupId;
                }
                else
                {
                    //30% what nonlcation would be chosen and 70 to location quests
                    subGroupId = GetRandomThroughDist(new List<int> { 30, 70 }) == 0
                        ? NonLocationGroupId
                        : groupId;
                }
            }

            var specificEncounters =
                gameSessionEncounters.Where(encounter => encounter.Id / 100 == subGroupId).ToList();
            if (specificEncounters.Count == 0)
            {
                //if game got in here something gone wrong, you are not supposed to see debug.log layed below
                Debug.Log("No encounters suitable");
            }

            var specificEncountersDistribution = specificEncounters.Select(encounter => encounter.Rate).ToList();
            var rand = GetRandomThroughDist(specificEncountersDistribution);
            var curEnc = specificEncounters.ElementAt(rand);
            if (curEnc == null)
            {
                Debug.Log("Cannot find encounter with id " + (subGroupId * 100 + rand));
                return null;
            }
            return curEnc;
        }
        private void GetCurrentEncounter()
        {

            if (currentEncCount < currentEncListCount - 1 || nextEncounter != null)
            {
                if (nextEncounter != null)
                {
                    currentEncounter = nextEncounter;
                    nextEncounter = null;
                }
                else
                {
                    //showing greetings description if we can
                    var startGameEncounter = gameSessionEncounters.Find(encounter => encounter.Id == groupId * 100);
                    currentEncounter = startGameEncounter ?? RandomCurrentEncounter();
                    gameSessionEncounters.Remove(currentEncounter);
                    UpdateLevelStatus();
                    Debug.Log("questID: " + currentEncounter.Id);
                    //showing store if needed

                }
                if (currentEncounter.Name == "Gamble_Play" && gambling == false)
                {
                    currentGambleBet = 5;
                    gambling = true;
                }
                if (currentEncounter.Name == "Store")
                {
                    ChangeState(GameSessionState.EnteringStore);
                    return;
                }
                ChangeState(GameSessionState.WaitingForPlayersChoice);
                descriptionText.text = currentEncounter.Description;
                var availableVariants = gameController.VariantsPool
                    .GetJoinByName(currentEncounter.VariantNames)
                    .Where(
                        variant =>
                            heroController.CheckEffectsAll(variant.NeededEffects) &&
                            heroController.CheckStatRequirements(variant.StatRequirements))
                    .ToList()
                    .Shuffle();
                AttachVariants(availableVariants);
            }
            else
            {
                EndGame(true);
            }
        }
        private void UpdateLevelStatus()
        {
            levelStatusText.text = ++currentEncCount + " / " + currentEncListCount;
        }

        private void AttachVariants(List<StandartVariant> availableVariants)
        {
            foreach (var button in variantButtons)
                button.GetComponent<ButtonHandler>().SetAttachedVariant(null);

            for (var i = 0; i < availableVariants.Count && i < variantButtons.Capacity; i++)
            {
                var btn = variantButtons[i];
                btn.GetComponent<ButtonHandler>().SetAttachedVariant(availableVariants[i]);
                btn.GetComponentInChildren<Text>().text = btn.GetComponent<ButtonHandler>().GetAttachedVariant().Description;
            }
        }

        public StandartResult DetermineResult(StandartVariant variant)
        {
            var resultsToChoose = gameController.ResultsPool.GetJoinByName(variant.Results)
                .Where(result => heroController.CheckEffectsAll(result.IncludedEffects)
                                      && heroController.CheckEffectsNone(result.ExcludedEffects))
                .ToList();
            Debug.Log("Choosing from "+ resultsToChoose.Count + "  results");
            var distributions = resultsToChoose.Select(result => result.Distribution);
            var distributionRates = distributions.Select(CalculateRate).ToList();
            var chosenResult = resultsToChoose[GetRandomThroughDist(distributionRates)];

            ChangeState(GameSessionState.ProcessingResult);
            return chosenResult;
        }

        private int CalculateRate(Distribution d)
        {
            var hero = heroController;
            var result = d.SuccessRate + hero.Str * d.StrMulti + hero.Int * d.IntMulti + hero.Stamina * d.StaminaMulti + hero.Kindness * d.KindMulti;
            return Mathf.RoundToInt(result);
        }

        private void EndGame(bool success)
        {
            RemoveResultPanel();
            isPlayerLost = !success;
            ShowLocationResult(success);
            ChangeState(GameSessionState.ProcessingEnding);
            heroController.CleanUpEffects();
        }
        /// <summary>
        /// returns the results of dice game with two dices
        /// first loser second is a winner
        /// </summary>
        /// <param name="first">first is loser</param>
        /// <param name="second">second is winner</param>
        private static void RandomDice(out int first, out int second)
        {
            first = Random.Range(1, 12);// [1..11]
            second = Random.Range(first + 1, 12); // [first..12]
        }

        private void InitResultPanel()
        {
            var prefab = Resources.Load("Prefab/ResultPanel") as GameObject;
            resultPanel = Instantiate(prefab, prefab.transform.position,
                prefab.transform.rotation, GameObject.Find("Canvas").transform);
            if (resultPanel != null) return;
            Debug.Log("Smth wrong in creating result panel");
            throw new NullReferenceException("Cannot instantiate result panel, try check out prefab");
        }
        public void ApplyQuestResult(StandartVariant variant)
        {
            if (resultPanel == null)
            {
                try
                {
                    InitResultPanel();
                }
                catch
                {
                    return;
                }
            }
            descriptionText.text = "";
            resultPanel.GetComponentInChildren<Text>().text = "";

            var result = DetermineResult(variant);
            result = new StandartResult(result);
            //raising bet
            if (variant.Name == "Gamble_Bet")
            {
                if (heroController.Gold == 0)
                {
                    result = gameController.ResultsPool.Find(res => res.Name == "Gamble_NotEnoughgold");
                }
                else
                {
                    if (heroController.Gold > currentGambleBet * 2)
                    {
                        heroController.ChangeGold(-currentGambleBet);
                        currentGambleBet *= 2;
                    }
                    else
                    {
                        currentGambleBet += heroController.Gold;
                        heroController.ChangeGold(-heroController.Gold);
                        resultPanel.GetComponentInChildren<Text>().text = "Ну, как говорится - олл-ин?\n";

                    }
                }
                Debug.Log("Gabmle_Bet chosen");
            }
            //throw dice
            if (variant.Name == "Gamble_Throw")
            {
                gambling = false;
                int mine, his;
                Debug.Log(result.Name);
                if (result.Tags.Contains("Win"))
                {
                    RandomDice(out his, out mine);
                    result.StatChange = new CharacteristicsChange(0, 0, currentGambleBet * 2, 0, -1);
                }
                else
                {
                    RandomDice(out mine, out his);
                }
                resultPanel.GetComponentInChildren<Text>().text = "Вы выбросили " + mine + " против " + his + "\n";

            }
            if (result.NextEncounterName != null)
            {
                nextEncounter =
                    gameController.EncountersPool.Find(encounter => encounter.Name == result.NextEncounterName);
            }
            var staminaIncrement = heroController.Str % 5;
            var staminaChange = (staminaIncrement > 0 ? staminaIncrement : 1) - result.StatChange.RequiredStamina;
            result.StatChange.RequiredStamina = gambling ? 0 : staminaChange;

            TryNotToLose(result);

            resultPanel.name = "ResultPanel";
            resultPanel.GetComponent<ResultPanelController>().ShowQuestResult(result);
        }

        private void TryNotToLose(StandartResult result)
        {
            if (!heroController.TryApplyStatChange(result.StatChange))
            {
                EndGame(false);
                return;
            }
            if (!heroController.AttachEffects(result.EffectsAdd))
            {
                EndGame(false);
                return;
            }
            if (!heroController.RemoveEffectsByName(result.EffectsRemove))
            {
                EndGame(false);
                return;
            }
        }
        private int CountScore()
        {
            var gameScore = heroController.Int + heroController.Str + heroController.Stamina / 10 +
                            heroController.Kindness / 10 + heroController.Effects.Count;
            return gameScore;
        }
        private void ShowLocationResult(bool success)
        {
            UpdateLevelStatus();
            if (success)
            {
                var score = CountScore();
                var successEndings = gameController.EndingsPool.Where(ending => ending.Tags.Contains("Success")).ToList();
                var maxScore = 0;
                var endingText = "";
                foreach (var ending in successEndings.Where(t => t.NeededScore <= score && maxScore < t.NeededScore))
                {
                    maxScore = ending.NeededScore;
                    endingText = ending.Description;
                }
                descriptionText.text = endingText;
            }
            else
            {
                var failEndings = gameController.EndingsPool.Where(ending => ending.Tags.Contains("Fail")).ToList();
                descriptionText.text = failEndings[(int)Random.value * (failEndings.Count - 1)].Description;
            }
        }

        public void RemoveResultPanel()
        {
            if (resultPanel == null)
            {
                Debug.Log("Result panel doesn't exist on screen");
                return;
            }
            resultPanel.SetActive(false);
        }
    }
}