﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.JSONData;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class StoreItemController : MonoBehaviour
    {
        private HeroController heroController;

        private Effect currentEffect;

        private Button buyBtn;
        private Text priceLbl;

        private EffectImageHandler effectImageController;
        bool initialized = false;

        public void InitWith(Effect effect)
        {
            currentEffect = effect;
            if (initialized)
            {
                buyBtn.interactable = true;
                effectImageController.Init(effect.Name, effect.Description);
                priceLbl.text = string.Format("{0} Gold", effect.Price);
            }
        }

        void Start()
        {
        }

        public void InitController()
        {
            heroController = GameObject.Find("Hero").GetComponent<HeroController>();

            effectImageController = transform.FindChild("EffectImage").GetComponent<EffectImageHandler>();

            priceLbl = transform.Find("PriceLbl").GetComponent<Text>();

            buyBtn = transform.Find("BuyBtn").GetComponent<Button>();
            buyBtn.onClick.AddListener(BuyBtnTapped);
            initialized = true;
            if (currentEffect != null)
                InitWith(currentEffect);
        }

        private void Update()
        {
        }

        public void BuyBtnTapped()
        {
            if (heroController.Gold >= currentEffect.Price)
            {
                if (heroController.AttachEffects(new List<string> {currentEffect.Name}))
                {
                    heroController.ChangeGold(-currentEffect.Price);
                }
                buyBtn.interactable = false;
            }
        }
    }
}