﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.UI;
using Assets.Scripts.JSONData;

namespace Assets.Scripts
{
    public class StoreController : MonoBehaviour
    {
        private const int conversionRate = 50;

        private GameSessionHandler sessionHandler;
        private HeroController heroController;
        private GameController gameController;
        private List<Effect> availableItems = new List<Effect>();
        private StoreItemController storeItem1;
        private StoreItemController storeItem2;

        private void Start()
        {
            sessionHandler = GameObject.Find("GameManager").GetComponent<GameSessionHandler>();
            gameController = GameObject.Find("GameManager").GetComponent<GameController>();
            heroController = GameObject.Find("Hero").GetComponent<HeroController>();

            GameObject.Find("StoreDoneBtn").GetComponent<Button>().onClick.AddListener(DoneBtnClick);
            GameObject.Find("GetGlobalBtn").GetComponent<Button>().onClick.AddListener(GetGlobalClick);
            GameObject.Find("GetRegularBtn").GetComponent<Button>().onClick.AddListener(GetRegularClick);

            storeItem1 = GameObject.Find("StoreItem1").GetComponent<StoreItemController>();
            storeItem2 = GameObject.Find("StoreItem2").GetComponent<StoreItemController>();
            storeItem1.InitController();
            storeItem2.InitController();
            Init();
        }

        public void Init()
        {
            if (gameController == null)
            {
                return;
            }
            availableItems.Clear();
            var items = gameController.EffectsPool.Where(e => e.Visible);
            if (!items.Any())
            {
                return;
            }
            foreach (var effect in items)
            {
                if (!heroController.Effects.Contains(effect))
                {
                    availableItems.Add(effect);
                }
            }

            var index = Random.Range(0, availableItems.Count);
            storeItem1.InitWith(availableItems[index]);
            availableItems.RemoveAt(index);

            index = Random.Range(0, availableItems.Count);
            storeItem2.InitWith(availableItems[index]);
        }

        private void Update()
        {
		
        }

        public void DoneBtnClick()
        {
            sessionHandler.ChangeState(GameSessionState.LeavingStore);
        }

        public void GetRegularClick()
        {
            if (heroController.GlobalGold >= 1)
            {
                heroController.ChangeGlobalGold(-1);
                heroController.ChangeGold(50);
            }
        }

        public void GetGlobalClick()
        {
            if (heroController.Gold >= conversionRate)
            {
                heroController.ChangeGlobalGold(1);
                heroController.ChangeGold(-50);
            }
        }
    }
}