﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class EffectImageHandler : MonoBehaviour
    {
        public void setDescription(string s)
        {
            gameObject.GetComponentInChildren<Text>().text = s;
            HideDescription();
        }

        public void Start()
        {
            var eventDown = new EventTrigger.Entry { eventID = EventTriggerType.PointerDown };
            eventDown.callback.AddListener(eventData => OnMouseDown());

            var eventUp = new EventTrigger.Entry { eventID = EventTriggerType.PointerUp };
            eventUp.callback.AddListener(eventData => OnMouseUp());
            
            gameObject.AddComponent<EventTrigger>().triggers.AddRange(new List<EventTrigger.Entry> { eventDown, eventUp });
        }

        public void Init(string effectName, string description)
        {
            var spr = Resources.Load<Sprite>(string.Format("EffectsIcon/{0}", effectName));
            GetComponent<Image>().sprite = spr;
            setDescription(description);
            name = effectName;
        }

        public void Init(string effectName, string description, Transform parent)
        {
            Init(effectName, description);
            transform.SetParent(parent);
        }

        public void Init(string effectName, string description, Transform parent, Vector2 position)
        {
            Init(effectName,description,parent);
            transform.localPosition = position;
        }
        public void OnMouseDown()
        {
            ShowDescription();
        }

        public void OnMouseUp()
        {
            HideDescription();
        }

        private void ShowDescription()
        {
            gameObject.GetComponentInChildren<Text>().enabled = true;
        }

        private void HideDescription()
        {
            gameObject.GetComponentInChildren<Text>().enabled = false;
        }
    }
}
