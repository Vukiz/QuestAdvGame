﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class HudController : MonoBehaviour
    {
        public GameObject EffectImagePrefab;

        /// <summary>
        /// current effects names
        /// </summary>
        private List<string> effectNames;

        private void Start()
        {
            GameObject.Find("Hero").GetComponent<HeroController>().OnEffectsChange += HandleEffectChanges;
        }

        public void StopShow()
        {
            gameObject.GetComponent<CanvasRenderer>().SetAlpha(0);
        }

        public void HandleEffectChanges(List<string> names)
        {
            RemoveExcessEffects(names); // удаляем из hud'a  все эффекты которые исчезли в новом списке имен "names"
            effectNames = names;
            ShowEffects();//показываем что осталось
        }

        private void RemoveExcessEffects(List<string> names)
        {
            if (effectNames == null || names == null)
                return;
            var removedItems = effectNames.Except(names).ToList();
            //все обьекты которые есть в старом списке но нет в новом
            foreach (var item in removedItems)
            {
                Destroy(transform.Find(item).gameObject);
            }
        }

        private void ShowEffects()
        {
            for (var i = 0; i < effectNames.Count; i++)
            {
                var imageName = effectNames.ElementAt(i);
                var currentImage = transform.Find(imageName);
                if (currentImage == null) //если картинки нет на экране
                {
                    currentImage = Instantiate(EffectImagePrefab).transform;
                    if (currentImage == null)
                        Debug.Log("cannot instantiate new effect image to effectHud");
                    else
                    {

                        currentImage.GetComponent<EffectImageHandler>()
                            .Init(imageName,
                            GameObject.Find("GameManager")
                                    .GetComponent<GameController>()
                                    .GetEffectDescription(imageName), transform);
                    }
                }
                currentImage.localPosition = new Vector2(GetComponent<RectTransform>().rect.width / 9 * i + 100,
                    GetComponent<RectTransform>().rect.height / 2);
            }
        }


    }
}
