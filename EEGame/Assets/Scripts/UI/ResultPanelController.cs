﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.JSONData;
using Assets.Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanelController : MonoBehaviour
{
    private GameController gameController;
    private GameObject effectImgPref;
    private HeroController heroController;

    public void ShowQuestResult(StandartResult result)
    {
        gameObject.SetActive(true);
        gameObject.transform.localPosition = new Vector2(0, 0);
        if (gameController == null)
            gameController = GameObject.Find("GameManager").GetComponent<GameController>();

        GetComponentInChildren<Text>().text += result.Description;

        if (effectImgPref == null)
            effectImgPref = Resources.Load("Prefab/EffectImage") as GameObject;
        ShowGold(result.StatChange.Gold);
        if (heroController == null)
            heroController = GameObject.Find("Hero").GetComponent<HeroController>();
        ShowAddedEffects(result.EffectsAdd.Where(effectName => !heroController.Effects.Select(heroEffect => heroEffect.Name).Contains(effectName)).ToList());// показать только те эффекты которых у героя уже не было
        ShowRemovedEffects(result.EffectsRemove.Where(effectName => heroController.Effects.Select(heroEffect => heroEffect.Name).Contains(effectName)).ToList());// показать только те эффекты что были у героя и удалены
        ShowStamina(result.StatChange.RequiredStamina);
        ShowKindness(result.StatChange.RequiredKindness);
    }

    private void ShowStamina(int staminaWasted)
    {
        var staminaText = transform.Find("Stamina").GetComponent<Text>();
        if (staminaWasted == 0)
        {
            staminaText.enabled = false;
            return;
        }
        staminaText.enabled = true;
        staminaText.text = staminaWasted > 0
            ? "Вы восстановили " + staminaWasted + " выносливости"
            : "Вы потратили " + staminaWasted * -1 + " выносливости";
    }

    private void ShowKindness(int kindness)
    {
        var kindnessText = transform.Find("Kindness").GetComponent<Text>();
        if (kindness == 0)
        {
            kindnessText.enabled = false;
            return;
        }
        kindnessText.enabled = true;
        kindnessText.text = kindness > 0
            ? "Вы получили " + kindness  + " доброжелательности"
            : "Вы потеряли " + kindness * -1 + " доброжелательности";
    }

    private void ShowGold(int gold)
    {
        var goldImgObj = gameObject.transform.Find("Goldsack");
        if (goldImgObj == null)
        {
            goldImgObj = Instantiate(effectImgPref).transform;
            goldImgObj.GetComponent<EffectImageHandler>().Init("Goldsack", "GoldDefaultStr", transform);
            goldImgObj.transform.localPosition = new Vector2(0, gameObject.transform.Find("AddedEffects").transform.localPosition.y);
        }
        if (gold != 0)
        {
            goldImgObj.GetComponent<EffectImageHandler>().setDescription(gold > 0 ? "Вы получили " + gold + " золота" : "Вы лишились " + gold + " золота");
        }
        goldImgObj.gameObject.SetActive(gold != 0);
    }

    private void ShowAddedEffects(List<string> effectNames)
    {
        foreach (Transform t in transform.Find("AddedEffects"))
        {
            Destroy(t.gameObject);
        }
        foreach (var effectName in effectNames)
        {
            var effectImage = Instantiate(effectImgPref);
            effectImage.GetComponent<EffectImageHandler>().Init(effectName, 
                gameController.GetEffectDescription(effectName),
                transform.Find("AddedEffects"));
            effectImage.transform.localPosition = new Vector2(25, gameObject.transform.Find("ResultDescription").transform.localPosition.y);
            return;
        }
    }

    private void ShowRemovedEffects(List<string> effectNames)
    {
        foreach (Transform t in transform.Find("RemovedEffects"))
        {
            Destroy(t.gameObject);
        }
        foreach (var effectName in effectNames)
        {
            var effectImage = Instantiate(effectImgPref);
            effectImage.GetComponent<EffectImageHandler>().Init(effectName, "Вы потеряли эффект " + effectName,
                transform.Find("RemovedEffects"));
            effectImage.transform.localPosition = new Vector2(0, gameObject.transform.Find("ResultDescription").transform.localPosition.y - 300);
            return;
        }
    }
}