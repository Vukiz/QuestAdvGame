﻿using Assets.Scripts.JSONData;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class ButtonHandler : MonoBehaviour
    {
        private StandartVariant attachedVariant;
        private GameSessionHandler sessionHandler;

        public ButtonHandler()
        {
        }

        public void Start()
        {
            sessionHandler = GameObject.Find("GameManager").GetComponent<GameSessionHandler>();
        }

        public void OnVariantClick()
        {
            sessionHandler.ApplyQuestResult(attachedVariant);
        }

        public void OnOkClick()
        {
            sessionHandler.ChangeState(GameSessionState.WaitingForEncounter);
            sessionHandler.RemoveResultPanel();
        }

        public void OnNewGameClick()
        {
            sessionHandler.ChangeState(GameSessionState.WaitingForStart);
            sessionHandler.StartNewGame();
        }

        public void OnExitClick()
        {
            Application.Quit();
        }

        public void SetAttachedVariant(StandartVariant variant)
        {
            attachedVariant = variant;
        }

        public StandartVariant GetAttachedVariant()
        {
            return attachedVariant;
        }
    }
}