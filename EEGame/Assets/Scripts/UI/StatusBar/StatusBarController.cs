﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Linq;

public class StatusBarController : MonoBehaviour
{
    public Text SubTitle;
    public Text Title;

    public int MaxValue { get; set; }

    public string IconName { get; set; }

    public string Name { get; set; }

    private int currentValue;

    public int Value
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (value <= MaxValue || MaxValue == 0)
            {
                currentValue = value;
            }
            UpdateText();
            if (currentValue < 0)
                throw new Exception(Name);
        }
    }

    private RectTransform fillPanelTransform;
    private RectTransform maskPanelTransform;

    void Start()
    {
        var maskPanel = transform.FindChild("MaskPanel");
        maskPanelTransform = maskPanel.GetComponent<RectTransform>();
        var maskPanelImage = maskPanelTransform.GetComponent<Image>();
        maskPanelTransform = maskPanelTransform.GetComponent<RectTransform>();

        var fillPanel = transform.FindChild("MaskPanel/FillPanel");
        fillPanelTransform = fillPanel.GetComponent<RectTransform>();

        maskPanelImage.sprite = Resources.Load<Sprite>(string.Format("Sprites/StatusBar/{0}", IconName));
     
        if (MaxValue == 0)
        {
            fillPanel.gameObject.SetActive(false);
        }
        UpdateText();
    }

    private void UpdateText()
    {
        if (fillPanelTransform != null && MaxValue != 0)
        {
            var height = maskPanelTransform.rect.height;
            var currentRatio = (float)Value / (float)MaxValue;
            var maskHeight = height * currentRatio;

            fillPanelTransform.localPosition = new Vector3(0, maskHeight, 0);
        }

        Title.text = Name;

        if (MaxValue == 0)
        {
            SubTitle.text = Value.ToString();
        }
        else
        {
            SubTitle.text = string.Format("{0}/{1}", Value, MaxValue);
        }
    }
}