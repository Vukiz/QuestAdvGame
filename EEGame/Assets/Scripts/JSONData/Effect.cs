namespace Assets.Scripts.JSONData
{
    [System.Serializable]
    public class Effect : BasicObject
    {
        public bool IsLongterm;

        public bool Visible; 

        public string[] EffectType;

        public int Price;

        public CharacteristicsChange EffectCharacteristics;
    }
}