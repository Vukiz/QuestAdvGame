﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.JSONData
{
    [Serializable]
    public class Hero : BasicObject
    {
        public List<string> InitialEffects;
        public int INT;
        public int STR;
        public int GOLD;
        public int STAMINA;
        public int KINDNESS;
    }
}
