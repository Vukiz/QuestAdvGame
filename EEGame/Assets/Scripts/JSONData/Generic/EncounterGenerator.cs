using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.JSONData.Standart;
using UnityEngine;

namespace Assets.Scripts.JSONData.Generic
{
    public class EncounterGenerator : MonoBehaviour
    {
        private List<GenericEncounter> encounters;
        private List<GenericVariant> variants;
//        private List<GenericResult> results;
        // Use this for initialization
        public void Start()
        {
            encounters = GameController.PullJSON<GenericEncounter>("GenericEncounters");
            variants = GameController.PullJSON<GenericVariant>("GenericVariants");
//            results = GameController.PullJSON<GenericResult>("GenericResults");
        }
	
        // Update is called once per frame
        public void Update()
        {
		
        }

        private GenericEncounter GetRandomEncounter()
        {
            return encounters[Random.Range(0, encounters.Count)];
        }

        private IEnumerable<GenericVariant> GetRandomVariantsWithTags(ICollection<string> tags, int amount)
        {
            //выбор всех вариантов с подходящими тегами
            var suitableVariants = (from variant in variants
                                    from tag in variant.Tags
                                    where tags.Contains(tag)
                                    select variant).ToList();
            var result = new List<GenericVariant>();
            //и выбор из подходящих <amount> различных вариантов
            for (var i = 0; i < amount; i++)
            {
                var item = suitableVariants[Random.Range(0, suitableVariants.Count)];
                result.Add(item);
                suitableVariants.Remove(item);
            }
            //отправка новых(не сохраненных) вариантов в GameController , чтобы была возможность вывести их на экран
            GameObject.Find("GameManager").GetComponent<GameController>().AddVariantsToGenPool(result);
            return result;
        }

        public Encounter GenerateEncounter(int variantsCount)
        {
            //выбор случайного енкаутера и копирование в результат(generatedEncounter)
            var generatedEncounter = new Encounter();
            var encounter = GetRandomEncounter();
            //TODO написать конструктор копирования для преобразования  GenericEncounter -> StandartEncounter || Сделать наследование StandartEncounter от GenericEncounter?
            generatedEncounter.Name = encounter.Name;
            generatedEncounter.Description = encounter.Description;

            //связывание generatedEncounter со случайной выборкой подходящих по тегам вариантов (преобразование GenericVariant'ов в Variant'ы и дополнение происходит внутри
            var tags = encounter.Tags;
            foreach (var variant in GetRandomVariantsWithTags(tags, variants.Count))
            {
                //TODO нужны ли нам имена у вариантов и результатов -> возможно удалить связи енкаунтеров(не генериков) с вариантами по именам и добвить для них теги
                generatedEncounter.VariantNames.Add(variant.Name);
            }
            
            //связывание generatedEncounter.Variants с Results
            foreach (var genVariant in generatedEncounter.VariantNames)
            {
                //genVariant;
            }
            return generatedEncounter;
        }
    }
}
