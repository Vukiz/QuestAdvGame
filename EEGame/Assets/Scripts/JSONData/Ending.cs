﻿

namespace Assets.Scripts.JSONData
{
    [System.Serializable]
    public class Ending : BasicObject
    {
        public int NeededScore;
    }
}
