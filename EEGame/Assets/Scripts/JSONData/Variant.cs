﻿using System.Collections.Generic;
using System;

namespace Assets.Scripts.JSONData
{
    [Serializable]
    public class Variant : BasicObject
    {
        public List<string> Results;
        public List<string> NeededEffects;
        public StatRequirements StatRequirements;
    }

    [Serializable]
    public class StatRequirements
    {
        public int INT;
        public int STR;
        public int GOLD;
        public int Stamina;
        public int Kindness;
    }
}