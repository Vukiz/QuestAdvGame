using System.Collections.Generic;
using System;
using Assets.Scripts.JSONData.Standart;

namespace Assets.Scripts.JSONData
{
    [Serializable]
    public class StandartResult : Result
    {
        public int SuccessRate;
        public List<string> EffectsRemove;
        public List<string> EffectsAdd;
        public List<string> IncludedEffects;
        public List<string> ExcludedEffects;
        public CharacteristicsChange StatChange;
        public Distribution Distribution;
        public string NextEncounterName;

        public StandartResult(StandartResult result)
        {
            Name = result.Name;
            Description = result.Description;
            Id = result.Id;
            Tags = result.Tags;
            SuccessRate = result.SuccessRate;
            EffectsRemove = result.EffectsRemove;
            EffectsAdd = result.EffectsAdd;
            IncludedEffects = result.IncludedEffects;
            ExcludedEffects = result.ExcludedEffects;
            StatChange = result.StatChange;
            Distribution = result.Distribution;
            NextEncounterName = result.NextEncounterName;
        }
    }

    [System.Serializable]
    public struct Distribution
    {
        public float IntMulti;
        public float StrMulti;
        public float StaminaMulti;
        public float KindMulti;
        public float SuccessRate;
    }

    [System.Serializable]
    public class CharacteristicsChange
    {
        public CharacteristicsChange(int i,int s,int g,int stamina,int kindness)
        {
            INT = i;
            STR = s;
            Gold = g;
            RequiredStamina = stamina;
            RequiredKindness = kindness;
        }
        public int INT;

        public int STR;

        public int Gold;

        public int RequiredStamina;

        public int RequiredKindness;
    }
}