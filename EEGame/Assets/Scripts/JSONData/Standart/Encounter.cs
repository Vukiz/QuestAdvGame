using System.Collections.Generic;

namespace Assets.Scripts.JSONData.Standart
{
    [System.Serializable]
    public class Encounter : BasicObject
    {
        public int Rate;
        public List<string> VariantNames;
        public string[] RelatedEffects;
    }
}