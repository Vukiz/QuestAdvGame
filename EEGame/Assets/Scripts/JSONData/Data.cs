using System.Collections.Generic;

namespace Assets.Scripts.JSONData
{
    public class BasicObject
    {
        public string Name;
        public string Description;
        public int Id;
        public List<string> Tags;
    }

    [System.Serializable]
    public class Data<T>
    {
        public List<T> Objects;
    }
}
