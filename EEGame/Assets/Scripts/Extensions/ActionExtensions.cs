﻿using System;

namespace Assets.Scripts
{
    public static class ActionExtensions
    {
        public static void SafeInvoke(this Action action)
        {
            if (action != null)
            {
                action.Invoke();
            }
        }

        public static void SafeInvoke<T>(this Action<T> action, T obj)
        {
            if (action != null)
            {
                action.Invoke(obj);
            }
        }
    }
}