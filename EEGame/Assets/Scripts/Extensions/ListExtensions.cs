﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.JSONData;
using UnityEngine;

namespace Assets.Scripts
{
    public static class ListExtensions
    {
        public static List<T> GetJoinByName<T>(this List<T> left, IEnumerable<string> right) where T : BasicObject
        {
            return left.Join(right, t => t.Name, n => n, (t, n) => t).ToList();
        }

        public static List<T> Shuffle<T>(this List<T> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                var temp = list[i];
                var randomIndex = Random.Range(0, list.Count);
                list[i] = list[randomIndex];
                list[randomIndex] = temp;
            }
            return list;
        }
    }
}